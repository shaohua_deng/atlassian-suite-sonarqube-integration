/*
 * SonarQube Integration for Confluence
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.confluence.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.UUID;

/**
 * @author Mark Rekveld
 * @since 1.0.0-beta-2
 */
public class SonarQubeWidgetMacro implements Macro {

	private static final String SPACEKEY_PARAM = "space";
	private static final String RESOURCE_PARAM = "resource";
	private static final String APPLICATION_PARAM = "application";
	private static final String WIDGET_PARAM = "widget";
	private static final String PROPERTIES_PARAM = "properties";
	private static final String WIDTH_PARAM = "width";
	private static final String BORDER_PARAM = "border";

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}

	@Override
	public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException {
		String widthValue = params.get(WIDTH_PARAM);
		if (StringUtils.isBlank(widthValue)) {
			widthValue = "450";
		}
		if (StringUtils.isNumeric(widthValue)) {
			widthValue += "px";
		}
		boolean border = true;
		if (params.get("border") != null) {
			border = Boolean.valueOf(params.get("border"));
		}
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		context.putAll(params);
		context.put("spacekey", params.containsKey(SPACEKEY_PARAM) ? params.get(SPACEKEY_PARAM) : conversionContext.getSpaceKey());
		context.put("macroId", UUID.randomUUID().toString().replaceAll("-", ""));
		context.put("width", widthValue);
		context.put("bordered", border);
		context.put("editable", ConversionContextOutputType.PREVIEW.value().equals(conversionContext.getOutputType()));
		return VelocityUtils.getRenderedTemplate("templates/sonarqube-widget-macro.vm", context);
	}

}
