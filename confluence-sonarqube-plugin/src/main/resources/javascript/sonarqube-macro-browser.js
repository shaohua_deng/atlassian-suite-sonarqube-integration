/*
 * SonarQube Integration for Confluence
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

AJS.namespace("SonarQubeMacro.browser");

SonarQubeMacro.browser.getSpaceKey = function () {
	return AJS.MacroBrowser.fields.space.getValue() || AJS.Meta.get("space-key");
}

SonarQubeMacro.browser.updateDashboards = function (input) {
	input = input || AJS.MacroBrowser.fields.dashboard.input;
	input.after("<span id='loading' class='aui-icon aui-icon-wait'>Wait</span>");
	AJS.$.ajax({
		url: AJS.contextPath() + "/rest/sqc/1.0/dashboards/" + SonarQubeMacro.browser.getSpaceKey() + "/primary",
		type: "GET",
		success: function (data) {
			input.empty();
			input.append(AJS.$("<option/>").attr("value", "-1").text("All"));
			AJS.$.each(data, function () {
				input.append(AJS.$("<option/>").attr("value", this.id).text(this.name));
			});
			input.val(input.data("original-value"));
			input.siblings("#loading").remove();
		},
		error: function () {
			input.siblings("#loading").remove();
		}
	});
};

SonarQubeMacro.browser.updateResources = function (input) {
	input = input || AJS.MacroBrowser.fields.resource.input;
	input.after("<span id='loading' class='aui-icon aui-icon-wait'>Wait</span>");
	AJS.$.ajax({
		url:  AJS.contextPath() + "/rest/sqc/1.0/entity/" + SonarQubeMacro.browser.getSpaceKey() + "/links",
		type: "GET",
		success: function (data) {
			input.empty();
			AJS.$.each(data.links, function () {
				input.append(AJS.$("<option/>").attr("value", this.key).text(this.name).data(this));
			});
			input.val(input.data("original-value"));
			input.siblings("#loading").remove();
			SonarQubeMacro.browser.resourceOnchange();
		},
		error: function () {
			input.siblings("#loading").remove();
		}
	});
};

SonarQubeMacro.browser.resourceOnchange = function () {
	var resource = AJS.MacroBrowser.fields.resource.input.find(":selected").data();
	AJS.MacroBrowser.fields.application.setValue(resource.applicationId);
	var input = AJS.MacroBrowser.fields.widget.input;
	input.after("<span id='loading' class='aui-icon aui-icon-wait'>Wait</span>");
	AJS.$.ajax({
		url: AJS.contextPath() + "/rest/sqc/1.0/widgets/" + resource.applicationId,
		type: "GET",
		success: function (data) {
			AJS.$.each(data, function () {
				input.append(AJS.$("<option/>").attr("value", this.id).text(this.title));
			});
			input.siblings("#loading").remove();
			input.val(input.data("original-value"));
			AJS.MacroBrowser.previewMacro(AJS.MacroBrowser.getMacroMetadata("sonarqube-widget"));
		},
		error: function () {
			input.siblings("#loading").remove();
		}
	});
};

AJS.MacroBrowser.setMacroJsOverride("sonarqube-panel", {
	"fields": {
		"spacekey": function (params, options) {
			options = options || {};
			options.onchange = SonarQubeMacro.browser.updateDashboards;
			var field = AJS.MacroBrowser.ParameterFields["spacekey"](params, options);
			field.input.keyup(function () {
				if (AJS.MacroBrowser.fields.space.getValue() == "") {
					AJS.log("Spacekey is emptied, trigger update of the dashboards selector");
					SonarQubeMacro.browser.updateDashboards();
				}
			});
			return field;
		},
		"int": function (params, options) {
			options = options || {};
			var paramDiv = AJS.$(Confluence.Templates.MacroBrowser.macroParameterSelect());
			var input = AJS.$("select", paramDiv);
			options.setValue = function (value) {
				this.input.data("original-value", value);
				SonarQubeMacro.browser.updateDashboards();
			};
			var field = AJS.MacroBrowser.Field(paramDiv, input, options);
			SonarQubeMacro.browser.updateDashboards(input);
			return field;
		}
	}
});

AJS.MacroBrowser.setMacroJsOverride("sonarqube-widget", {
	"manipulateMarkup": function (macro) {
		if(AJS.MacroBrowser.widgetPropertiesChanged){
			var container = top.document.getElementById("macro-insert-container");
			var propertiesInput = $("#macro-param-div-properties input", container);
			propertiesInput.val(SonarQubeCommon.serializeProperties(AJS.MacroBrowser.widgetProperties));
		}
	},
	"fields": {
		"spacekey": function (params, options) {
			options = options || {};
			var field = AJS.MacroBrowser.ParameterFields["spacekey"](params, options);
			field.input.keyup(function () {
				if (AJS.MacroBrowser.fields.space.getValue() == "") {
					AJS.log("Spacekey is emptied, trigger update of the resource selector");
					SonarQubeMacro.browser.updateResources();
				}
			});
			return field;
		},
		"string": function (params, options) {
			var paramDiv, input, field;
			switch (params.name) {
				case "application":
				case "properties":
					return AJS.MacroBrowser.ParameterFields["_hidden"](params, options);
				case "resource":
					options = options || {};
					options.onchange = SonarQubeMacro.browser.resourceOnchange;
					paramDiv = AJS.$(Confluence.Templates.MacroBrowser.macroParameterSelect());
					input = AJS.$("select", paramDiv);
					options.setValue = function (value) {
						this.input.data("original-value", value);
						SonarQubeMacro.browser.updateResources();
					};
					field = AJS.MacroBrowser.Field(paramDiv, input, options);
					SonarQubeMacro.browser.updateResources(input);
					return field;
				case "widget":
					options = options || {};
					paramDiv = AJS.$(Confluence.Templates.MacroBrowser.macroParameterSelect());
					input = AJS.$("select", paramDiv);
					options.setValue = function (value) {
						this.input.data("original-value", value);
					};
					field = AJS.MacroBrowser.Field(paramDiv, input, options);
					return field;
				default:
					return AJS.MacroBrowser.ParameterFields["string"](params, options);
			}
		}
	}
});
