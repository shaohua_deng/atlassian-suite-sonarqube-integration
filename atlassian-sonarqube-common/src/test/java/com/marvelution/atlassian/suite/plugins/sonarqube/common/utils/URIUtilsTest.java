/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.utils;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for {@link URIUtils}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class URIUtilsTest {

	/**
	 * Test for {@link URIUtils#encode(String, String)}
	 *
	 * @throws UnsupportedEncodingException
	 */
	@Test
	public void testEncode() throws UnsupportedEncodingException {
		assertThat(URIUtils.encode("with a space", "UTF-8"), is("with%20a%20space"));
	}

	/**
	 * Test for {@link URIUtils#appendPathsToURI(java.net.URI, String...)}
	 */
	@Test
	public void testAppendPathsToURI() {
		assertThat(URIUtils.appendPathsToURI(URI.create("https://marvelution.atlassian.net/"), "rest", "applinks", "1.0", "manifest"),
				is(URI.create("https://marvelution.atlassian.net/rest/applinks/1.0/manifest")));
	}

}
