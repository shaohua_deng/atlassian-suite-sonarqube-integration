/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

AJS.namespace("SonarQubeGadget");

SonarQubeGadget.descriptor = function (gadget, args, baseUrl) {
	var entities = AJS.$.map(args.entities.entities, function (entity) {
		return {
			label: entity.name,
			value: entity.key
		};
	});
	gadgets.window.setTitle(gadget.getMsg("sonarqube.gadget.title"));
	return  {
		action: "",
		theme: function () {
			if (gadgets.window.getViewportDimensions().width < 450) {
				return "gdt top-label";
			} else {
				return "gdt";
			}
		}(),
		fields: [
			{
				userpref: "localKey",
				label: args.i18n.entityLabel,
				description: args.i18n.entityDescription,
				id: "userpref_localKey_id",
				type: "select",
				selected: gadget.getPref("localKey"),
				options: entities
			},
			{
				userpref: "remoteKey",
				label: gadget.getMsg("sonarqube.gadget.remote.entity.label"),
				id: "userpref_remote_container",
				type: "callbackBuilder",
				callback: function (parentDiv) {
					parentDiv.append(
							AJS.$("<input/>", {
								id: "userpref_applicationId_id",
								type: "hidden",
								name: "applicationId"
							}).val(gadget.getPref("applicationId"))
						).append(
							AJS.$("<select/>", {
								id: "userpref_remoteKey_id",
								name: "remoteKey"
							}).addClass("select")
						).append(AJS.$("<div/>").addClass("description")
							.text(gadget.getMsg("sonarqube.gadget.remote.entity.description")));
					SonarQubeGadget.remoteEntityOptions(gadget, AJS.$("#userpref_localKey_id").val(), args.i18n.entityLabel);
					AJS.$("#userpref_localKey_id").change(function () {
						SonarQubeGadget.remoteEntityOptions(gadget, AJS.$(this).val(), args.i18n.entityLabel);
					});
				}
			},
			{
				userpref: "widgetId",
				label: gadget.getMsg("sonarqube.gadget.widget.label"),
				id: "userpref_widget_container",
				type: "callbackBuilder",
				callback: function (parentDiv) {
					parentDiv.append(
							AJS.$("<select/>", {
								id: "userpref_widgetId_id",
								name: "widgetId"
							}).addClass("select")
						).append(
							AJS.$("<div/>").addClass("description").text(gadget.getMsg("sonarqube.gadget.widget.description"))
						);
					SonarQubeGadget.widgetOptions(gadget, gadget.getPref("applicationId"));
					AJS.$("#userpref_applicationId_id").change(function () {
						SonarQubeGadget.widgetOptions(gadget, AJS.$(this).val());
					});
				}
			},
			{
				userpref: "properties",
				type: "hidden",
				value: gadget.getPref("properties")
			},
			{
				userpref: "isConfigured",
				type: "hidden",
				value: "true"
			}
		]
	};
};

SonarQubeGadget.remoteEntityOptions = function (gadget, localKey, remoteLabel) {
	if (localKey != undefined && localKey.length > 0) {
		AJS.$.ajax({
			url: "/rest/sqc/1.0/entity/" + localKey + "/links",
			type: "GET",
			success: function (data) {
				var selector = AJS.$("#userpref_remoteKey_id").empty();
				AJS.$.each(data.links, function () {
					selector.append(
						AJS.$("<option/>", {
							value: this.key
						}).text(this.name).data(this)
					);
				});
				if (gadget.getPref("remoteKey") !== "") {
					selector.val(gadget.getPref("remoteKey"));
				} else {
					var link = selector.find(":selected").data();
					AJS.$("#userpref_applicationId_id").val(link.applicationId).change();
				}
			},
			error: function () {
				gadget.showMessage("error", gadget.getMsg("sonarqube.gadget.remote.entity.load.error", remoteLabel), true, true);
			}
		});
	}
};

SonarQubeGadget.widgetOptions = function (gadget, applicationId) {
	if (applicationId != undefined && applicationId.length > 0) {
		AJS.$.ajax({
			url: "/rest/sqc/1.0/widgets/" + applicationId,
			type: "GET",
			success: function (data) {
				var selector = AJS.$("#userpref_widgetId_id").empty();
				AJS.$.each(data, function () {
					selector.append(
						AJS.$("<option/>", {
							value: this.id
						}).text(this.title).data(this)
					);
				});
				if (gadget.getPref("widgetId") !== "") {
					selector.val(gadget.getPref("widgetId"));
				}
			},
			error: function () {
				gadget.showMessage("error", gadget.getMsg("sonarqube.gadget.widget.load.error"), true, true);
			}
		});
	}
};

SonarQubeGadget.template = function (gadget, args, baseUrl) {
	gadget.getView().empty();
	var editable = false;
	try {
		editable = gadget.getConfig !== undefined;
	} catch (e) {}
	gadget.getView().append(SonarQubeWidget.template({
		baseUrl: baseUrl,
		key: gadget.getPref("widgetId"),
		entity: gadget.getPref("localKey"),
		remoteEntity: gadget.getPref("remoteKey"),
		remoteApplication: gadget.getPref("applicationId"),
		properties: SonarQubeCommon.deserializeProperties(gadget.getPref("properties")),
		propertiesChanged: function (properties) {
			gadget.savePref("properties", SonarQubeCommon.serializeProperties(properties));
		},
		editable: editable,
		width: gadget.getView().width(),
		frameload: function (title) {
			AJS.log("Frameload trigger received for gadget: " + gadget.getPref("widgetId"));
			gadgets.window.setTitle(title);
			gadget.resize();
		}
	}));
};
