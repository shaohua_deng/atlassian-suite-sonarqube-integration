/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.google.common.collect.ImmutableMap;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.Communicator;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.CommunicatorFactory;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Proxy REST Service for SonarQube calls
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@AnonymousAllowed
@Path("dashboards")
public class DashboardResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(DashboardResource.class);
	private final CommunicatorFactory communicatorFactory;
	private final HostApplicationFacade hostApplicationFacade;
	private final AuthenticationConfigurationManager authenticationConfigurationManager;

	/**
	 * Constructor
	 *
	 * @param communicatorFactory   the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.services.CommunicatorFactory}
	 * @param hostApplicationFacade the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade}
	 * @param authenticationConfigurationManager
	 *                              the {@link com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager}
	 */
	public DashboardResource(CommunicatorFactory communicatorFactory, HostApplicationFacade hostApplicationFacade,
	                         AuthenticationConfigurationManager authenticationConfigurationManager) {
		this.communicatorFactory = communicatorFactory;
		this.hostApplicationFacade = hostApplicationFacade;
		this.authenticationConfigurationManager = authenticationConfigurationManager;
	}

	/**
	 * Proxy a GET request for the primary SonarQube resource link of a local entity
	 *
	 * @param localKey              the key of the local project/space
	 * @param authenticationContext the {@link AuthenticationContext}
	 * @return the response form the target
	 */
	@GET
	@Path("{localKey}/primary")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dashboardsForPrimaryLink(@PathParam("localKey") String localKey,
	                                         @Context AuthenticationContext authenticationContext) {
		EntityLink primary = hostApplicationFacade.getEntityLink(localKey);
		if (primary != null) {
			verifyAccessToApplication(primary.getApplicationLink().getId(), authenticationContext);
			String service = "/api/dashpanels/all?format=json&resource=" + primary.getKey();
			LOGGER.debug("Getting all Dashboards resource {} on SonarQube with id {}", primary.getKey(),
					primary.getApplicationLink().getId());
			return getDashboards(primary.getApplicationLink().getId(), service);
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No primary SonarQube Resource configured for " + localKey).build();
		}
	}

	/**
	 * Get all global dashboards for a specific application
	 *
	 * @param applicationId         the ApplicationId of the target SonarQube instance
	 * @param authenticationContext the {@link AuthenticationContext}
	 * @return the response form the target
	 */
	@GET
	@Path("{applicationId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGlobalDashboards(@PathParam("applicationId") ApplicationId applicationId,
	                                    @Context AuthenticationContext authenticationContext) {
		verifyAccessToApplication(applicationId, authenticationContext);
		String service = "/api/dashpanels/all?format=json";
		LOGGER.debug("Getting all Global Dashboards for SonarQube with id {}", applicationId.get());
		return getDashboards(applicationId, service);
	}

	private Response getDashboards(ApplicationId applicationId, String service) {
		try {
			Communicator communicator = communicatorFactory.get(applicationId);
			try {
				RestResponse response = communicator.get(service, ImmutableMap.<String,
						String>of("X-Requested-With", "XMLHttpRequest"));
				return Response.status((response.isSuccessful()) ? Response.Status.OK.getStatusCode() : response.getStatusCode()).entity
						(response.getResponseBody()).build();
			} catch (CredentialsRequiredException e) {
				throw new WebApplicationException(Response.Status.UNAUTHORIZED);
			}
		} catch (IllegalArgumentException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}

	private void verifyAccessToApplication(ApplicationId applicationId, AuthenticationContext authenticationContext) {
		if (authenticationConfigurationManager.isConfigured(applicationId, BasicAuthenticationProvider.class) && !authenticationContext
				.isAuthenticated()) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
	}

}
