/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services.impl;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.applinks.api.application.jira.JiraProjectEntityType;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReference;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReferences;

import javax.annotation.Nullable;

/**
 * JIRA Specific implementation for {@link AbstractHostApplicationFacade}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JIRAApplicationFacade extends AbstractHostApplicationFacade {

	private final ProjectManager projectManager;
	private final PermissionManager permissionManager;
	private final JiraAuthenticationContext authenticationContext;
	private final Function<Project, EntityReference> projectToEntityReference = new Function<Project, EntityReference>() {
		@Override
		public EntityReference apply(@Nullable Project project) {
			if (project != null && canView(project)) {
				return new EntityReference(project.getKey(), project.getName());
			}
			return null;
		}
	};

	/**
	 * Constructor
	 *
	 * @param applicationLinkService the {@link com.atlassian.applinks.api.ApplicationLinkService}
	 * @param typeAccessor           the Applinks {@link com.atlassian.applinks.spi.util.TypeAccessor}
	 * @param entityLinkService      the {@link com.atlassian.applinks.api.EntityLinkService}
	 * @param projectManager         the {@link com.atlassian.jira.project.ProjectManager}
	 * @param permissionManager      the {@link com.atlassian.jira.security.PermissionManager}
	 * @param authenticationContext  the {@link JiraAuthenticationContext}
	 */
	public JIRAApplicationFacade(ApplicationLinkService applicationLinkService, TypeAccessor typeAccessor,
	                             EntityLinkService entityLinkService, ProjectManager projectManager, PermissionManager permissionManager,
	                             JiraAuthenticationContext authenticationContext) {
		super(applicationLinkService, entityLinkService, typeAccessor, JiraApplicationType.class, JiraProjectEntityType.class);
		this.projectManager = projectManager;
		this.permissionManager = permissionManager;
		this.authenticationContext = authenticationContext;
	}

	@Override
	protected Object getRawEntity(String key) {
		return projectManager.getProjectObjByKey(key);
	}

	@Override
	public EntityReference getEntity(String key) {
		return projectToEntityReference.apply(projectManager.getProjectObjByKey(key));
	}

	@Override
	public EntityReferences getEntities() {
		return new EntityReferences(Iterables.transform(projectManager.getProjectObjects(), projectToEntityReference));
	}

	@Override
	public boolean isAuthenticated() {
		return authenticationContext.getLoggedInUser() != null;
	}

	@Override
	public boolean isSystemAdministrator() {
		return isAuthenticated() && permissionManager.hasPermission(Permissions.ADMINISTER, authenticationContext.getLoggedInUser());
	}

	/**
	 * Permission check for {@link Project}
	 *
	 * @param project the {@link Project} to check for
	 * @return {@code true} is the current user is an administrator, project administrator, or can browse the project
	 */
	private boolean canView(Project project) {
		return permissionManager.hasPermission(Permissions.PROJECT_ADMIN, project, authenticationContext.getLoggedInUser()) ||
				permissionManager.hasPermission(Permissions.BROWSE, project, authenticationContext.getLoggedInUser());
	}

}
