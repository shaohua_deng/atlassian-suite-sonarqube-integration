/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.rest.filter;

import com.atlassian.plugins.rest.common.security.AuthenticationRequiredException;
import com.atlassian.plugins.rest.common.security.AuthorisationException;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.rest.security.AdminRequired;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

import javax.ws.rs.ext.Provider;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * {@link javax.ws.rs.ext.Provider} implementation to check for the {@link AdminRequired} annotation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Provider
public class AdminRequiredResourceFilter implements ResourceFilter, ContainerRequestFilter {

	private final AbstractMethod abstractMethod;
	private final HostApplicationFacade hostApplicationFacade;

	/**
	 * Constructor
	 *
	 * @param abstractMethod        the {@link AbstractMethod} being invoked
	 * @param hostApplicationFacade the {@link HostApplicationFacade}
	 */
	public AdminRequiredResourceFilter(AbstractMethod abstractMethod, HostApplicationFacade hostApplicationFacade) {
		this.abstractMethod = checkNotNull(abstractMethod);
		this.hostApplicationFacade = checkNotNull(hostApplicationFacade);
	}

	@Override
	public ContainerRequestFilter getRequestFilter() {
		return this;
	}

	@Override
	public ContainerResponseFilter getResponseFilter() {
		return null;
	}

	@Override
	public ContainerRequest filter(ContainerRequest request) {
		if (isAdminRequired()) {
			if (!hostApplicationFacade.isAuthenticated()) {
				throw new AuthenticationRequiredException();
			} else if (!hostApplicationFacade.isSystemAdministrator()) {
				throw new AuthorisationException();
			}
		}
		return request;
	}

	/**
	 * Check if the {@link AdminRequired} annotation is present for this {@link AbstractMethod}
	 *
	 * @return {@code true} if the {@link AdminRequired} annotation is present on the method or resource
	 */
	private boolean isAdminRequired() {
		return ((abstractMethod != null) && (abstractMethod.isAnnotationPresent(AdminRequired.class) ||
				abstractMethod.getResource().isAnnotationPresent(AdminRequired.class)));
	}

}
