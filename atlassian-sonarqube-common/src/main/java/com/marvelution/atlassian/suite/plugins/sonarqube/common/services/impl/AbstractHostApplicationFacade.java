/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services.impl;

import com.atlassian.applinks.api.*;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.applinks.SonarQubeResourceEntityLinkType;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade;

import javax.annotation.Nullable;

/**
 * Base implementation for the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade} interface
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class AbstractHostApplicationFacade implements HostApplicationFacade {

	private final ApplicationLinkService applicationLinkService;
	private final EntityLinkService entityLinkService;
	private final TypeAccessor typeAccessor;
	private final Class<? extends ApplicationType> applicationType;
	private final Class<? extends EntityType> entityType;

	/**
	 * Constructor
	 *
	 * @param applicationLinkService the {@link ApplicationLinkService}
	 * @param entityLinkService      the {@link com.atlassian.applinks.api.EntityLinkService}
	 * @param typeAccessor           the Applinks {@link com.atlassian.applinks.spi.util.TypeAccessor}
	 * @param applicationType        the local {@link com.atlassian.applinks.api.ApplicationType} Class
	 * @param entityType             the local {@link com.atlassian.applinks.api.EntityType} Class
	 */
	protected AbstractHostApplicationFacade(ApplicationLinkService applicationLinkService, EntityLinkService entityLinkService,
	                                        TypeAccessor typeAccessor, Class<? extends ApplicationType> applicationType,
	                                        Class<? extends EntityType> entityType) {
		this.applicationLinkService = applicationLinkService;
		this.entityLinkService = entityLinkService;
		this.typeAccessor = typeAccessor;
		this.applicationType = applicationType;
		this.entityType = entityType;
	}

	@Override
	public final ApplicationType getApplicationType() {
		return typeAccessor.getApplicationType(applicationType);
	}

	@Override
	public ApplicationLink getApplicationLink(ApplicationId applicationId) {
		try {
			return applicationLinkService.getApplicationLink(applicationId);
		} catch (TypeNotInstalledException e) {
			return null;
		}
	}

	@Override
	public final EntityType getEntityType() {
		return typeAccessor.getEntityType(entityType);
	}

	@Override
	public EntityLink getEntityLink(String localKey) {
		return getEntityLink(localKey, null, null);
	}

	@Override
	public EntityLink getEntityLink(String localKey, final @Nullable ApplicationId applicationId, final @Nullable String remoteKey) {
		Iterable<EntityLink> links = entityLinkService.getEntityLinks(getRawEntity(localKey), SonarQubeResourceEntityLinkType.class);
		Predicate<EntityLink> predicate;
		if ((applicationId == null || remoteKey == null) && Iterables.size(links) == 1) {
			return links.iterator().next();
		} else if (applicationId == null || remoteKey == null) {
			predicate = new Predicate<EntityLink>() {
				@Override
				public boolean apply(@Nullable EntityLink entityLink) {
					return entityLink != null && entityLink.isPrimary();
				}
			};
		} else {
			predicate = new Predicate<EntityLink>() {
				@Override
				public boolean apply(@Nullable EntityLink entityLink) {
					return entityLink != null && applicationId.equals(entityLink.getApplicationLink().getId()) && remoteKey.equals
							(entityLink.getKey());
				}
			};
		}
		try {
			return Iterables.find(links, predicate);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public Iterable<EntityLink> getEntityLinks(String localKey, @Nullable final ApplicationId applicationId) {
		Iterable<EntityLink> links = entityLinkService.getEntityLinks(getRawEntity(localKey), SonarQubeResourceEntityLinkType.class);
		if (applicationId == null) {
			return links;
		} else {
			return Iterables.filter(links, new Predicate<EntityLink>() {
				@Override
				public boolean apply(@Nullable EntityLink entityLink) {
					return entityLink != null && entityLink.getApplicationLink().getId().equals(applicationId);
				}
			});
		}
	}

	/**
	 * Get the raw local entity for the given key
	 *
	 * @param key the local entity key
	 * @return the local entity
	 */
	protected abstract Object getRawEntity(String key);

}
