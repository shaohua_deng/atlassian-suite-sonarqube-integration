/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityType;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.model.*;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade;

import javax.annotation.Nullable;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReference} resource
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@AnonymousAllowed
@Path("entity")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED })
@Produces({ MediaType.APPLICATION_JSON })
public class EntityResource {

	private final HostApplicationFacade hostApplicationFacade;
	private final I18nResolver i18nResolver;

	/**
	 * Constructor
	 *
	 * @param hostApplicationFacade the {@link HostApplicationFacade}
	 * @param i18nResolver          the {@link I18nResolver}
	 */
	public EntityResource(HostApplicationFacade hostApplicationFacade, I18nResolver i18nResolver) {
		this.hostApplicationFacade = hostApplicationFacade;
		this.i18nResolver = i18nResolver;
	}

	/**
	 * Get the texts for the local entity type
	 *
	 * @return the {@link LocalTypeI18n} with text Strings
	 */
	@GET
	@Path("i18n")
	public LocalTypeI18n getI18n() {
		EntityType entityType = hostApplicationFacade.getEntityType();
		return new LocalTypeI18n(i18nResolver.getText(entityType.getShortenedI18nKey()), i18nResolver.getText(entityType.getI18nKey()));
	}

	/**
	 * Get a single {@link EntityReference} by key
	 *
	 * @param key hte key of the {@link EntityReference} to get
	 * @return the {@link EntityReference}, may be {@code null} in case there is no entity with the specified key,
	 *         of the current user doesn't have permissions to view it.
	 */
	@GET
	@Path("{key}")
	public EntityReference getEntity(@PathParam("key") String key) {
		EntityReference entity = hostApplicationFacade.getEntity(key);
		if (entity != null) {
			return entity;
		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	/**
	 * Get all the SonarQube entity links for the given local entity
	 *
	 * @param key           the key of the local entity
	 * @param applicationId the application id of the SonarQube instance to filter with, may be {@code null}
	 * @return the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReferences},
	 *         may be {@code empty} in case the current user doesn't have permissions to view any entity.
	 */
	@GET
	@Path("{key}/links")
	public EntityLinkReferences getEntityLinks(@PathParam("key") String key, @QueryParam("applicationId") ApplicationId applicationId) {
		Iterable<EntityLink> links = hostApplicationFacade.getEntityLinks(key, applicationId);
		return new EntityLinkReferences(Iterables.transform(links, new Function<EntityLink, EntityLinkReference>() {
			@Override
			public EntityLinkReference apply(@Nullable EntityLink link) {
				if (link == null) {
					return null;
				} else {
					return new EntityLinkReference(link.getKey(), link.getName(), link.getApplicationLink().getId().get(),
							link.isPrimary());
				}
			}
		}));
	}

	/**
	 * Get all {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReferences}
	 *
	 * @return the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.model.EntityReferences},
	 *         may be {@code empty} in case the current user doesn't have permissions to view any entity.
	 */
	@GET
	public EntityReferences getEntities() {
		return hostApplicationFacade.getEntities();
	}

}
