/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.rest.filter;

import com.google.common.collect.Lists;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import javax.ws.rs.ext.Provider;
import java.util.List;

/**
 * {@link javax.ws.rs.ext.Provider} for the {@link com.sun.jersey.spi.container.ResourceFilterFactory}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Provider
public class AdminRequiredResourceFilterFactory implements ResourceFilterFactory {

	private final HostApplicationFacade hostApplicationFacade;

	/**
	 * Constructor
	 *
	 * @param hostApplicationFacade the {@link HostApplicationFacade} implementation
	 */
	public AdminRequiredResourceFilterFactory(HostApplicationFacade hostApplicationFacade) {
		this.hostApplicationFacade = hostApplicationFacade;
	}

	@Override
	public List<ResourceFilter> create(AbstractMethod abstractMethod) {
		return Lists.<ResourceFilter>newArrayList(new AdminRequiredResourceFilter(abstractMethod, hostApplicationFacade));
	}

}
