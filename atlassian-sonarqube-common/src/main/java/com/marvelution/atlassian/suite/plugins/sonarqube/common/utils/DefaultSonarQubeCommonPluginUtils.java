/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.utils;

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;

import java.net.MalformedURLException;

/**
 * Default Plugin Utility implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultSonarQubeCommonPluginUtils implements SonarQubeCommonPluginUtils {

	private final String pluginKey;
	private final Version version;

	/**
	 * Default constructor
	 *
	 * @param bundleContext the {@link org.osgi.framework.BundleContext}
	 */
	public DefaultSonarQubeCommonPluginUtils(BundleContext bundleContext) throws MalformedURLException {
		Bundle bundle = bundleContext.getBundle();
		pluginKey = OsgiHeaderUtil.getPluginKey(bundle);
		version = bundle.getVersion();
	}

	@Override
	public String getPluginKey() {
		return pluginKey;
	}

	@Override
	public Version getVersion() {
		return version;
	}

}
