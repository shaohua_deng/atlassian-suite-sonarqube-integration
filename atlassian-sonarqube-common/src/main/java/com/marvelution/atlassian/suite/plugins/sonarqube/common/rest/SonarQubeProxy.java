/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.google.common.collect.ImmutableMap;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.Communicator;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.CommunicatorFactory;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.HostApplicationFacade;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * Proxy REST Service for SonarQube calls
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@AnonymousAllowed
@Path("proxy")
public class SonarQubeProxy {

	private static final Logger LOGGER = LoggerFactory.getLogger(SonarQubeProxy.class);
	private final CommunicatorFactory communicatorFactory;
	private final HostApplicationFacade hostApplicationFacade;
	private final AuthenticationConfigurationManager authenticationConfigurationManager;

	/**
	 * Constructor
	 *
	 * @param communicatorFactory   the {@link com.marvelution.atlassian.suite.plugins.sonarqube.common.services.CommunicatorFactory}
	 * @param hostApplicationFacade the {@link HostApplicationFacade}
	 * @param authenticationConfigurationManager
	 *                              the {@link com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager}
	 */
	public SonarQubeProxy(CommunicatorFactory communicatorFactory, HostApplicationFacade hostApplicationFacade,
	                      AuthenticationConfigurationManager authenticationConfigurationManager) {
		this.communicatorFactory = communicatorFactory;
		this.hostApplicationFacade = hostApplicationFacade;
		this.authenticationConfigurationManager = authenticationConfigurationManager;
	}

	/**
	 * Proxy a GET request for the primary SonarQube resource link of a local entity
	 *
	 * @param localKey the key of the local project/space
	 * @param route    the url to proxy
	 * @param uriInfo  the current {@link UriInfo}
	 * @return the response form the target
	 */
	@GET
	@Path("{localKey}/primary/{route: .+}")
	@Produces(MediaType.WILDCARD)
	public Response proxyPrimaryGet(@PathParam("localKey") String localKey, @PathParam("route") String route,
	                                @Context UriInfo uriInfo, @Context AuthenticationContext authenticationContext) {
		EntityLink primary = hostApplicationFacade.getEntityLink(localKey);
		if (primary != null) {
			return proxyGet(primary.getApplicationLink().getId(), route, uriInfo, authenticationContext);
		} else {
			return Response.status(Response.Status.NOT_FOUND).entity("No primary SonarQube Resource configured for " + localKey).build();
		}
	}

	/**
	 * Proxy a GET request for a specific remote application
	 *
	 * @param applicationId the ApplicationId of the target SonarQube instance
	 * @param route         the url to proxy
	 * @param uriInfo       the current {@link UriInfo}
	 * @return the response form the target
	 */
	@GET
	@Path("{applicationId}/{route: .+}")
	@Produces(MediaType.WILDCARD)
	public Response proxyGet(@PathParam("applicationId") ApplicationId applicationId, @PathParam("route") String route,
	                         @Context UriInfo uriInfo, @Context AuthenticationContext authenticationContext) {
		if (authenticationConfigurationManager.isConfigured(applicationId, BasicAuthenticationProvider.class) && !authenticationContext
				.isAuthenticated()) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
		LOGGER.debug("Proxying /{}?{} for SonarQube with id {}", new String[] { route, uriInfo.getRequestUri().getQuery(),
				applicationId.get() });
		try {
			Communicator communicator = communicatorFactory.get(applicationId);
			try {
				RestResponse response = communicator.get(String.format("/%s?%s", route, uriInfo.getRequestUri().getQuery()),
						ImmutableMap.<String, String>of("X-Requested-With", "XMLHttpRequest"));
				return Response.status((response.isSuccessful()) ? Response.Status.OK.getStatusCode() : response.getStatusCode()).entity
						(response.getResponseBody()).build();
			} catch (CredentialsRequiredException e) {
				throw new WebApplicationException(Response.Status.UNAUTHORIZED);
			}
		} catch (IllegalArgumentException e) {
			return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
	}

}
