/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.services.impl;

import com.atlassian.applinks.api.*;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.Maps;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.Communicator;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.RestResponse;
import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.util.Map;

/**
 * Default {@link Communicator}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultCommunicator implements Communicator {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCommunicator.class);
	public static final int CALL_TIMEOUT = 50000;
	private final AuthenticationConfigurationManager authenticationConfigurationManager;
	private final ApplicationLink applicationLink;

	/**
	 * Constructor
	 *
	 * @param authenticationConfigurationManager
	 *                        the {@link AuthenticationConfigurationManager}
	 * @param applicationLink the {@link ApplicationLink}
	 */
	public DefaultCommunicator(AuthenticationConfigurationManager authenticationConfigurationManager, ApplicationLink applicationLink) {
		this.authenticationConfigurationManager = authenticationConfigurationManager;
		this.applicationLink = applicationLink;
	}

	@Override
	public RestResponse get(String service) throws CredentialsRequiredException {
		return get(service, CALL_TIMEOUT);
	}

	@Override
	public RestResponse get(String service, Map<String, String> headers) throws CredentialsRequiredException {
		return get(service, headers, CALL_TIMEOUT);
	}

	@Override
	public RestResponse get(String service, int timeout) throws CredentialsRequiredException {
		return get(service, Maps.<String, String>newHashMap(), timeout);
	}

	@Override
	public RestResponse get(String service, Map<String, String> headers, int timeout) throws CredentialsRequiredException {
		LOGGER.debug("Invoking service {} in applink {} with timeout of {}", new Object[] { service, applicationLink.getName(), timeout });
		return executeGetRequest(service, headers, timeout);
	}

	/**
	 * Getter for the {@link com.atlassian.applinks.api.ApplicationLinkRequestFactory}
	 *
	 * @return the {@link com.atlassian.applinks.api.ApplicationLinkRequestFactory}
	 */
	private ApplicationLinkRequestFactory getAuthenticatedRequestFactory() {
		return applicationLink.createAuthenticatedRequestFactory();
	}

	/**
	 * Execute a Get Request on the {@link ApplicationLink} given
	 *
	 * @param url     the target services url
	 * @param headers request headers
	 * @param timeout timeout in milliseconds
	 * @return the {@link RestResponse} response
	 * @throws com.atlassian.applinks.api.CredentialsRequiredException
	 *          in case the {@link ApplicationLink} credentials are outdated
	 */
	private RestResponse executeGetRequest(String url, Map<String, String> headers, int timeout) throws CredentialsRequiredException {
		ApplicationLinkRequestFactory requestFactory = getAuthenticatedRequestFactory();
		ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, url);
		for (Map.Entry<String, String> header : headers.entrySet()) {
			request.addHeader(header.getKey(), header.getValue());
		}
		return executeRequest(request, requestFactory, timeout);
	}

	/**
	 * Execute the given {@link com.atlassian.applinks.api.ApplicationLinkRequest}
	 *
	 * @param request        the {@link com.atlassian.applinks.api.ApplicationLinkRequest} to execute
	 * @param requestFactory the {@link ApplicationLinkRequestFactory} used to create the request
	 * @param timeout        timeout in milliseconds
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	private RestResponse executeRequest(ApplicationLinkRequest request, ApplicationLinkRequestFactory requestFactory,
	                                    int timeout) throws CredentialsRequiredException {
		request.setConnectionTimeout(timeout);
		request.setSoTimeout(timeout);
		return executeRequest(request, requestFactory);
	}

	/**
	 * Execute the given {@link ApplicationLinkRequest}
	 *
	 * @param request        the {@link ApplicationLinkRequest} to execute
	 * @param requestFactory the {@link ApplicationLinkRequestFactory} used to create the request
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	private RestResponse executeRequest(ApplicationLinkRequest request, ApplicationLinkRequestFactory requestFactory)
			throws CredentialsRequiredException {
		try {
			// All calls are anonymous if there is no Remote User, so always add the basic auth header if it is configured
			if (authenticationConfigurationManager.isConfigured(applicationLink.getId(),
					BasicAuthenticationProvider.class)) {
				Map<String, String> config = authenticationConfigurationManager.getConfiguration(applicationLink.getId(),
						BasicAuthenticationProvider.class);
				request.addBasicAuthentication(config.get("username"), config.get("password"));
			}
			RestResponse response = request.execute(new ApplicationLinkResponseHandler<RestResponse>() {

				@Override
				public RestResponse credentialsRequired(Response response) throws ResponseException {
					return new CredentialsRequiredRestResponse(response);
				}

				@Override
				public RestResponse handle(Response response) throws ResponseException {
					if (response.getStatusCode() == HttpStatus.SC_FORBIDDEN || response.getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
						return credentialsRequired(response);
					}
					return new RestResponse(response);
				}

			});
			if (response instanceof CredentialsRequiredRestResponse) {
				throw new CredentialsRequiredException(requestFactory, "Unable to authenticate");
			} else {
				return response;
			}
		} catch (ResponseException e) {
			if (e.getCause() instanceof ConnectException) {
				return new RestResponse("Unable to connect to Jenkins. Connection timed out.");
			} else {
				return new RestResponse("Failed to execute request: " + e.getMessage());
			}
		}
	}

	/**
	 * Credentials Required specific {@link RestResponse}
	 */
	private class CredentialsRequiredRestResponse extends RestResponse {

		/**
		 * Constructor
		 *
		 * @param response the {@link com.atlassian.sal.api.net.Response} to base hte CredentialsRequiredRestResponse on
		 */
		private CredentialsRequiredRestResponse(Response response) {
			super(response);
		}

	}

}
