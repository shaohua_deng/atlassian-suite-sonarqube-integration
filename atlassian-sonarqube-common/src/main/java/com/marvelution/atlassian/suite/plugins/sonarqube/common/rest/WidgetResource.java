/*
 * SonarQube Integration Commons
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.common.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.Communicator;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.CommunicatorFactory;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.RestResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Widget Resource to get widget data from a remove SonarQube instance
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Path("widgets")
public class WidgetResource {

	private final CommunicatorFactory communicatorFactory;

	/**
	 * Constructor
	 *
	 * @param communicatorFactory   the {@link CommunicatorFactory}
	 */
	public WidgetResource(CommunicatorFactory communicatorFactory) {
		this.communicatorFactory = communicatorFactory;
	}

	/**
	 * Get all the Widgets that are available on the target SonarQube instance specified by the ApplicationId
	 *
	 * @param applicationId the ApplicationId of the target SonarQube
	 * @return JSON collection of Widget details
	 */
	@GET
	@Path("{applicationId}")
	@Consumes({ MediaType.WILDCARD, MediaType.APPLICATION_FORM_URLENCODED })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getWidgets(@PathParam("applicationId") ApplicationId applicationId) {
		try {
			Communicator communicator = communicatorFactory.get(applicationId);
			RestResponse response = communicator.get("/api/widgets");
			if (response.isSuccessful()) {
				return Response.ok(response.getResponseBody(), MediaType.APPLICATION_JSON_TYPE).build();
			} else {
				throw new WebApplicationException(response.getStatusCode());
			}
		} catch (Exception e) {
			throw new WebApplicationException(e, Response.Status.NOT_FOUND);
		}
	}

}
