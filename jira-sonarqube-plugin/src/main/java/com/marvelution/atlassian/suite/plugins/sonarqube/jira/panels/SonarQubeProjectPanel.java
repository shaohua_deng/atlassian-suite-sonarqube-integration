/*
 * SonarQube Integration for JIRA
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.jira.panels;

import com.atlassian.jira.plugin.projectpanel.impl.AbstractProjectTabPanel;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.marvelution.atlassian.suite.plugins.sonarqube.common.services.DashboardPanelRenderer;

/**
 * {@link AbstractProjectTabPanel} implementation to display SonarQube Dashboards
 *
 * @author Mark Rekveld
 * @since 1.0.0-beta-2
 */
public class SonarQubeProjectPanel extends AbstractProjectTabPanel {

	private final PermissionManager permissionManager;
	private final DashboardPanelRenderer dashboardPanelRenderer;

	/**
	 * Default Constructor
	 *
	 * @param permissionManager      the {@link PermissionManager}
	 * @param dashboardPanelRenderer the {@link DashboardPanelRenderer}
	 */
	public SonarQubeProjectPanel(PermissionManager permissionManager, DashboardPanelRenderer dashboardPanelRenderer) {
		this.permissionManager = permissionManager;
		this.dashboardPanelRenderer = dashboardPanelRenderer;
	}

	@Override
	public String getHtml(BrowseContext ctx) {
		return dashboardPanelRenderer.getPanel(ctx.getProject().getKey());
	}

	@Override
	public boolean showPanel(BrowseContext browseContext) {
		return permissionManager.hasPermission(Permissions.VIEW_VERSION_CONTROL, browseContext.getProject(), browseContext.getUser());
	}

}
