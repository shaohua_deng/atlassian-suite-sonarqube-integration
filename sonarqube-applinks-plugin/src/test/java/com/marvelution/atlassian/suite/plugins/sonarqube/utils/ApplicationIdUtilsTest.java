/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.utils;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for {@link ApplicationIdUtils}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ApplicationIdUtilsTest {

	public static final String MARVELUTION_ISSUES_ID = "27274ce6-d606-3a64-b0bb-df07fa4b4d00";
	public static final String HTTPS_MARVELUTION_ATLASSIAN_NET = "https://marvelution.atlassian.net";
	private ApplicationIdUtils idUtils;

	/**
	 * Setup the tests
	 */
	@Before
	public void setup() {
		idUtils = new ApplicationIdUtils();
	}

	/**
	 * Test {@link ApplicationIdUtils#getApplicationId(String)}
	 */
	@Test
	public void testGetApplicationId() {
		assertThat(idUtils.getApplicationId("http://localhost:9000"), is("0a0611b4-2c0d-30ad-85d9-791f5fe3c9a6"));
	}

	/**
	 * Test {@link ApplicationIdUtils#verifyRemoteApplicationId(String, String)}
	 */
	@Test
	public void testVerifyRemoteApplicationId() {
		assertThat(idUtils.verifyRemoteApplicationId(MARVELUTION_ISSUES_ID, HTTPS_MARVELUTION_ATLASSIAN_NET), is(true));
	}

	/**
	 * Test {@link ApplicationIdUtils#verifyRemoteApplicationId(String, String)}
	 */
	@Test
	public void testVerifyRemoteApplicationIdNotValid() {
		assertThat(idUtils.verifyRemoteApplicationId("FAKE", HTTPS_MARVELUTION_ATLASSIAN_NET), is(false));
	}

	/**
	 * Test {@link ApplicationIdUtils#verifyRemoteApplicationId(String, String)}
	 */
	@Test
	public void testVerifyRemoteApplicationIdRemoteNotReachable() {
		assertThat(idUtils.verifyRemoteApplicationId(MARVELUTION_ISSUES_ID, "http://i.dont.exist"), is(false));
	}

}
