/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.filter;

import com.marvelution.atlassian.suite.plugins.sonarqube.BaseITCase;
import org.junit.Test;

/**
 * Integration tests for the Streams feature
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class StreamsITCase extends BaseITCase {

	/**
	 * Test case for {@link /rest/activity-streams/1.0/config?local=true}
	 *
	 * @throws Exception
	 */
	@Test
	public void testConfiguration() throws Exception {

	}

	/**
	 * Test case for {@link /plugins/servlet/streams?streams=key+IS+org.codehaus.sonar%3Aexample-java-maven+org.codehaus
	 * .sonar%3Aexample-ut-maven-cobertura-runTests&sonarqube=category+IS+Alert+Version&local=true&use-accept-lang=true&providers
	 * =sonarqube&maxResults=5}
	 *
	 * @throws Exception
	 */
	@Test
	public void testFeedWithUser() throws Exception {

	}

	/**
	 * Test case for {@link /plugins/servlet/streams?streams=key+IS+org.codehaus.sonar%3Aexample-java-maven+org.codehaus
	 * .sonar%3Aexample-ut-maven-cobertura-runTests&sonarqube=category+IS+Alert+Version&local=true&use-accept-lang=true&providers
	 * =sonarqube&maxResults=5}
	 *
	 * @throws Exception
	 */
	@Test
	public void testFeedWithoutUser() throws Exception {

	}

}
