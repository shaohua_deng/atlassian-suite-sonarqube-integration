/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.filter;

import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.marvelution.atlassian.suite.plugins.sonarqube.BaseITCase;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration test for the {@link ApplinksAuthConfServletFilter}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ApplinksAuthConfServletFilterITCase extends BaseITCase {

	/**
	 * Verify that the auth form requests return a not supported message
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testBasicAuthForm() throws Exception {
		HtmlPage htmlPage = webClient.getPage("http://localhost:9000/plugins/servlet/applinks/auth/conf/basic.html");
		assertThat(htmlPage.getTitleText(), is("Application Link Auth Configuration"));
		assertThat(htmlPage.getBody().getTextContent(), is("\nNot yet supported."));
	}

}
