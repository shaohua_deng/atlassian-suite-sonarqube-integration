/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.junit.Before;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.CSSParseException;
import org.w3c.css.sac.ErrorHandler;
import org.w3c.dom.Element;

/**
 * Base test case for integration tests
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class BaseITCase {

	protected WebClient webClient;

	/**
	 * Setup the test case
	 */
	@Before
	public void setup() {
		webClient = new WebClient();
		webClient.getOptions().setJavaScriptEnabled(false);
		webClient.setCssErrorHandler(new ErrorHandler() {
			@Override
			public void warning(CSSParseException e) throws CSSException {
			}

			@Override
			public void error(CSSParseException e) throws CSSException {
			}

			@Override
			public void fatalError(CSSParseException e) throws CSSException {
			}
		});
	}

	/**
	 * Login to the SonarQube test instance
	 *
	 * @param user the username and password to login with
	 * @throws Exception
	 */
	protected void login(String user) throws Exception {
		HtmlPage page = webClient.getPage("http://localhost:9000/sessions/new");
		HtmlForm form = page.getForms().get(0);
		form.getInputByName("login").setValueAttribute(user);
		form.getInputByName("password").setValueAttribute(user);
		form.getInputByName("commit").click();
	}

	/**
	 * Helper to get the text content of an XML Element
	 *
	 * @param element the main element
	 * @param name    the name of the sub element to get the content of
	 * @return the content
	 */
	protected String getElementContent(Element element, String name) {
		return element.getElementsByTagName(name).item(0).getTextContent();
	}

}
