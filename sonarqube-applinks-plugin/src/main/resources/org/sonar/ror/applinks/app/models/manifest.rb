#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class Manifest

  attr_reader :name, :type_id, :id
  attr_accessor :outbound_auth_types, :inbound_auth_types, :build_number, :icon_url, :applinks_version, :version, :url, :public_signup

  def initialize(id, name)
    @id = id
    @name = name
    @type_id = 'sonarqube'
    @inbound_auth_types = 'com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider'
    @outbound_auth_types = 'com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider'
  end

  def to_xml(xml = Builder::XmlMarkup.new(:indent => 0))
    xml.instruct!
    xml.manifest do
      xml.id(@id)
      xml.name(@name)
      xml.typeId(@type_id)
      xml.version(@version)
      xml.buildNumber(@build_number) if @build_number
      xml.applinksVersion(@applinks_version)
      xml.inboundAuthenticationTypes(@inbound_auth_types)
      xml.outboundAuthenticationTypes(@outbound_auth_types)
      if @public_signup
        xml.publicSignup('true')
      else
        xml.publicSignup('false')
      end
      xml.url(@url)
      xml.iconUrl(@icon_url)
    end
  end

end