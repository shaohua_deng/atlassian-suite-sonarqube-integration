#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class Api::DashpanelsController < Api::ApiController

  def all
    global = !params[:resource]

    dashboards = Dashboard.all(:conditions => ['(shared=? or user_id=?) and is_global=?', true, current_user.id, global])
    dashboards = Api::Utils.insensitive_sort(dashboards, &:name)

    respond_to do |format|
      format.json { render :json => dashboards_to_json(dashboards) }
      format.xml  { render :xml => dashboards_to_xml(dashboards) }
      format.text { render :text => text_not_supported }
    end
  end

  def index
    begin
      dashboard = Dashboard.find(params[:id])

      respond_to do |format|
        format.json { render :json => dashboard_to_json(dashboard) }
        format.xml  { render :xml => dashboard_to_xml(dashboard) }
        format.text { render :text => text_not_supported }
      end

    rescue ActiveRecord::RecordNotFound => e
      render_error(e.message, 404)
    rescue ApiException => e
      render_error(e.msg, e.code)
    rescue Exception => e
      logger.error("Fails to execute #{request.url} : #{e.message}")
      render_error(e.message)
    end
  end

  private

  def dashboards_to_xml(dashboards, xml = Builder::XmlMarkup.new(:indent => 0))
    xml.dashboards do
      dashboards.each do |dashboard|
        dashboard_to_xml(dashboard, xml)
      end
    end
  end

  def dashboard_to_xml(dashboard, xml = Builder::XmlMarkup.new(:indent => 0))
    xml.dashboard do
      xml.id(dashboard.id.to_s)
      xml.name(dashboard.name(true))
      xml.description(dashboard.description) if dashboard.description
      xml.shared(dashboard.shared)
      xml.column_layout(dashboard.column_layout)
      columns = dashboard.column_layout.split('-')
      xml.columns do
        for index in 1..columns.size()
          xml.column do
            xml.width(columns[index - 1])
            xml.widgets do
              dashboard.widgets.select { |widget| widget.column_index==index }.sort_by { |widget| widget.row_index }.each do |widget|
                xml.widget do
                  xml.key(widget.widget_key)
                  xml.properties do
                    widget.properties.each do |prop|
                      xml.property do
                        xml.key(prop.key)
                        xml.value { xml.cdata!(prop.text_value) }
                      end
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  def dashboards_to_json(dashboards = [])
    json = []
    dashboards.each do |dashboard|
      json << dashboard_to_json(dashboard)
    end
    json
  end

  def dashboard_to_json(dashboard)
    hash = {}
    hash[:id] = dashboard.id.to_s
    hash[:name] = dashboard.name(true)
    hash[:description] = dashboard.description if dashboard.description
    hash[:shared] = dashboard.shared
    hash[:column_layout] = dashboard.column_layout
    columns = dashboard.column_layout.split('-')
    hash[:columns] = []
    for index in 1..columns.size()
      column = {}
      column[:width] = columns[index - 1]
      column[:widget] = []
      dashboard.widgets.select { |widget| widget.column_index==index }.sort_by { |widget| widget.row_index }.each do |widget|
        w = {}
        w[:key] = widget.widget_key
        w[:properties] = []
        widget.properties.each do |prop|
          w[:properties] << prop.to_hash_json
        end
        column[:widget] << w
      end
      hash[:columns] << column
    end
    hash
  end

end
