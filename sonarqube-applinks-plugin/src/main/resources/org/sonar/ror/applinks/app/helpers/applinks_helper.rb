#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Main plugin helper
module ApplinksHelper

  include ApplicationHelper

  # Returns the SonarQube instance base url
  def base_url
    Property.value(applinks_settings::APPLINKS_URL, nil, applinks_settings::APPLINKS_URL_DEFAULT_VALUE)
  end

  # Returns the SonarQube instance application name
  def applinks_name
    Property.value(applinks_settings::APPLINKS_APP_NAME, nil, applinks_settings::APPLINKS_APP_NAME_DEFAULT_VALUE)
  end

  # Returns an Applink ID for the given base_url, defaults to the base url of the SonarQube instance
  def applinks_id(url = base_url)
    get_component('com.marvelution.atlassian.suite.plugins.sonarqube.utils.ApplicationIdUtils').getApplicationId(url)
  end

  # Verifies that the given ID matches the given remote application url
  # And returns a true of false
  def verify_remote_application_id(id, url)
    get_component('com.marvelution.atlassian.suite.plugins.sonarqube.utils.ApplicationIdUtils').verifyRemoteApplicationId(id, url)
  end

  # Returns the version of the applinks API that is implemented
  def applinks_version
    '${atlassian.applinks.runtime.version}'
  end

  # Returns the SonarQube plugin key
  def plugin_key
    '${sonarqube.plugin.key}'
  end

  # Returns the ApplinksSettings module class for statics access
  def applinks_settings
    get_component('com.marvelution.atlassian.suite.plugins.sonarqube.ApplinksSettings').class
  end

  # Returns a component for the given component name
  def get_component(component)
    Api::Utils.java_facade.getComponentByClassname(plugin_key, component)
  end

  # Helper method to l10n
  def self.message(key, options={})
    Api::Utils.message(key, options)
  end

  # Returns all the projects that the current user has access to then can also be filtered by the given resource keys and inclusion flag
  def load_projects(resource_keys = nil, inclusion = true)
    projects = load_projects_no_auth_check(resource_keys, inclusion)
    select_authorized(:user, projects)
  end

  # Returns all the projects available on the SonarQube instance without an auth check
  def load_projects_no_auth_check(resource_keys = nil, inclusion = true)
    conditions = ['enabled = :enabled AND scope IN (:scopes) AND qualifier IN (:qualifiers)']
    values = {
        :enabled => true,
        :scopes => ['PRJ'],
        :qualifiers => ['TRK']
    }
    unless resource_keys.nil?
      if inclusion
        conditions << 'kee IN (:keys)'
      else
        conditions << 'kee NOT IN (:keys)'
      end
      values[:keys] = resource_keys
    end
    Project.find(:all, :select => 'id,kee,name,language,long_name,scope,qualifier,root_id',
                 :conditions => [conditions.join(' AND '), values], :order => 'name')
  end

end
