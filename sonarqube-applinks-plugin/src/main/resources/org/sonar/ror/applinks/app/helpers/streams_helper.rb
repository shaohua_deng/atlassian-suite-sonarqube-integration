#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Helper for the Streams Controller
module StreamsHelper

  include ApplinksHelper
  include SourceHelper

  # Returns a stream entry from an event
  def get_entry_from_event(event)
    unless event.nil?
      params = [event.name, event.resource.name(true)]
      title = message('applinks.streams.' + event.category.downcase + '.title', :params => params, :default => 'unknown')
      title_as_text = message('applinks.streams.' + event.category.downcase + '.title_as_text', :params => params, :default => 'unknown')
      if title.eql?('unknown')
        title = message('applinks.streams.event.title', :params => params)
        title_as_text = message('applinks.streams.event.title_as_text', :params => params)
      end
      entry = StreamEntry.new(get_entry_id([event.id.to_s, event.name, event.event_date.to_s]), title, title_as_text, event.event_date,
                              project_url(event.resource))
      unless event.description.nil?
        entry.content = render_blockquote_content([event.description])
      end
      entry.category = message('event.category.' + event.category)
      entry.icon = qualifier_icon_link(event.resource)
      entry
    end
  end

  # Returns a stream entry from an issue
  def get_entry_from_issue(issue, reporter)
    unless issue.nil?
      if reporter.nil?
        reporter_name = message('applinks.analysis')
      else
        reporter_name = reporter.name
      end
      resource = Project.by_key(issue.componentKey())
      title = render_title({:user => reporter, :issue => issue, :issue_url => issue_url(issue), :resource => resource,
                            :resource_url => resource_url(resource), :action => 'applinks.issue.raised', :relation => 'applinks.on'})
      title_as_text = message('applinks.issue.raised.title_as_text', :params => [reporter_name, issue.message, resource.name(true)])
      entry = StreamEntry.new(get_entry_id([issue.key]), title, title_as_text, issue.creationDate(), issue_url(issue))
      entry.icon = qualifier_icon_link(resource)
      entry.category = message('applinks.issue.raised')
      unless reporter.nil?
        entry.author = {
            :name => reporter.name,
            :username => reporter.login,
            :email => reporter.email
        }
      end
      if resource.last_snapshot and issue.line
        snapshot = resource.last_snapshot
        panel = get_html_source_panel(snapshot, {:line_range => (issue.line-5)..(issue.line+5), :highlighted_lines => [issue.line]})
        entry.content = render_to_string :layout => false, :partial => 'source',
                                         :locals => {:display_manual_violation_form => false,
                                                     :scm_available => nil,
                                                     :display_coverage => nil,
                                                     :lines => panel.html_lines,
                                                     :expanded => nil,
                                                     :display_violations => nil,
                                                     :display_issues => nil,
                                                     :resource => nil,
                                                     :snapshot => nil,
                                                     :review_screens_by_vid => false,
                                                     :filtered => panel.filtered}
      end
      entry
    end
  end

  # Returns a stream entry from an issue changelog
  def get_entry_from_issue_changelog(issue, change, user)
    unless issue.nil? and changelog.nil?
      resource = Project.by_key(issue.componentKey())
      title = render_title({:user => user, :issue => issue, :issue_url => issue_url(issue), :resource => resource,
                            :resource_url => resource_url(resource), :action => 'applinks.issue.updated'})
      title_as_text = message('applinks.issue.updated.title_as_text', :params => [user.name, issue.message, resource.name(true)])
      entry = StreamEntry.new(get_entry_id([issue.key, change.id]), title, title_as_text, change.createdAt(), issue_url(issue))
      entry.category = message('applinks.issue.updated')
      entry.icon = qualifier_icon_link(resource)
      content = []
      change.diffs.entrySet().each do |difference|
        msg = ''
        field = message(difference.getKey())
        diff = difference.getValue()
        if diff.newValue.present?
          msg << message('issue.changelog.changed_to', :params => [field, diff.newValue()])
        else
          msg << message('issue.changelog.removed', :params => [field])
        end
        if diff.oldValue.present?
          msg << ' (' + message('issue.changelog.was', :params => [diff.oldValue]) + ')'
        end
        content << msg
      end
      entry.content = render_blockquote_content(content)
      entry.author = {
          :name => user.name,
          :username => user.login,
          :email => user.email
      }
      entry
    end
  end

  # Returns a stream entry from an issue comment
  def get_entry_from_issue_comment(issue, comment, user)
    unless issue.nil? and comment.nil?
      resource = Project.by_key(issue.componentKey())
      title = render_title({:user => user, :issue => issue, :issue_url => issue_url(issue), :resource => resource,
                            :resource_url => resource_url(resource), :action => 'applinks.issue.commented'})
      title_as_text = message('applinks.issue.commented.title_as_text', :params => [user.name, issue.message, resource.name(true)])
      entry = StreamEntry.new(get_entry_id([issue.key, comment.key]), title, title_as_text, comment.createdAt(), issue_url(issue))
      entry.category = message('applinks.issue.commented')
      entry.icon = qualifier_icon_link(resource)
      entry.content = render_blockquote_content(Internal.text.markdownToHtml(comment.markdownText))
      entry.author = {
          :name => user.name,
          :username => user.login,
          :email => user.email
      }
      entry
    end
  end

  # Returns an xml representation of the given stream feed
  def feed_to_xml(feed, xml = Builder::XmlMarkup.new(:indent => 0))
    xml.instruct!
    xml.feed(:xmlns => 'http://www.w3.org/2005/Atom', 'xmlns:atlassian' => 'http://streams.atlassian.com/syndication/general/1.0') do
      xml.id(feed.link)
      xml.link(:href => feed.link, :rel => 'self')
      xml.title(feed.title, :type => 'text')
      xml.updated(feed.updated.utc.iso8601)
      xml.tag!('atlassian:timezone-offset') do
        xml.text!(feed.updated.formatted_offset)
      end
      unless feed.entries.nil?
        feed.entries.each do |entry|
          entry_to_xml(entry, xml)
        end
      end
    end
  end

  private

  # Returns an xml representation of the given stream entry
  def entry_to_xml(entry, xml = Builder::XmlMarkup.new(:indent => 0))
    xml.entry('xmlns:activity' => 'http://activitystrea.ms/spec/1.0/') do
      xml.id('urn:uuid:' + entry.uuid)
      xml.title(entry.title, :type => 'html')
      xml.content(entry.content, :type => 'html') unless entry.content.nil?
      xml.author('xmlns:usr' => 'http://streams.atlassian.com/syndication/username/1.0') do
        xml.tag!('activity:object-type') do
          xml.text!('http://activitystrea.ms/schema/1.0/person')
        end
        if entry.author.nil?
          xml.name(applinks_name)
          xml.url(base_url)
        else
          xml.name(entry.author[:name])
          #xml.url(base_url + '/user/admin')
          xml.email(entry.author[:email])
          xml.tag!('usr:username') do
            xml.text!(entry.author[:username])
          end
        end
        xml.link('xmlns:media' => 'http://purl.org/syndication/atommedia', :rel => 'photo',
                 :href => base_url + url_for_static(:plugin => plugin_key, :path => 'images/icon16_sonarqube.png'),
                 'media:height' => '16', 'media:width' => '16')
        xml.link('xmlns:media' => 'http://purl.org/syndication/atommedia', :rel => 'photo',
                 :href => base_url + url_for_static(:plugin => plugin_key, :path => 'images/icon48_sonarqube.png'),
                 'media:height' => '48', 'media:width' => '48')
      end
      xml.published(entry.published.utc.iso8601)
      xml.updated(entry.published.utc.iso8601)
      xml.category(:term => entry.category)
      xml.generator(:uri => base_url)
      xml.tag!('atlassian:application') do
        xml.text!('com.sonarsource.sonarqube')
      end
      xml.link(:rel => 'alternate', :href => entry.link)
      xml.link(:rel => 'http://streams.atlassian.com/syndication/css', :href => base_url + url_for_static(:plugin => plugin_key,
                                                                                                         :path => 'styles/streams.css'))
      unless entry.icon.nil?
        xml.link(:rel => 'http://streams.atlassian.com/syndication/icon', :href => entry.icon[:href], :title => entry.icon[:title])
      end
      xml.tag!('activity:verb') do
        xml.text!('http://activitystrea.ms/schema/1.0/update')
      end
      xml.tag!('activity:object') do
        xml.id('urn:uuid:' + entry.uuid)
        xml.title(entry.title_as_text, :type => 'text')
        xml.link(:rel => 'alternate', :href => entry.link)
        xml.tag!('activity:object-type') do
          xml.text!('http://activitystrea.ms/schema/1.0/status')
        end
      end
      xml.tag!('atlassian:timezone-offset') do
        xml.text!(entry.published.formatted_offset)
      end
    end
  end

  # Returns the url for the given issue
  def issue_url(issue)
    base_url + '/issue/show/' + issue.key
  end

  # Returns the url for the given reosurce
  def resource_url(resource)
    base_url + '/resource/index/' + resource.key + '?layout=false&tab=issues'
  end

  # Returns the url for the given project
  def project_url(project)
    base_url + '/dashboard/index/' + project.id.to_s
  end

  # Returns an UUID build from the given elements
  def get_entry_id(elements)
    Java::JavaUtil::UUID.nameUUIDFromBytes(elements.join(':').to_s.bytes.to_a).toString()
  end

  # Returns the qualifier icon (href and title) for the object given
  def qualifier_icon_link(object)
    qualifier=(object.respond_to?('qualifier') ? object.qualifier : object.to_s)
    if qualifier
      {:href => base_url + Java::OrgSonarServerUi::JRubyFacade.getInstance().getResourceType(qualifier).getIconPath(),
       :title => message('qualifier.' + qualifier)}
    end
  end

  # Returns the html rendered title with the given locals variables
  def render_title(locals)
    locals[:base_url] = base_url
    locals[:relation] = 'applinks.of' if locals[:relation].nil?
    render_to_string :layout => false, :partial => 'title', :locals => locals
  end

  # Returns the html rendered blockquote content with the given locals variables
  def render_blockquote_content(quotes)
    render_to_string :layout => false, :partial => 'blockquote', :locals => {:quotes => quotes}
  end

  # Returns if the given user is allowed by the given filter
  def user_allowed_by_filter(user, filter)
    if filter.nil? or user.nil?
      # No filter so allow
      result = true
    else
      if filter[:operator].eql?IS_OPERATOR[:key]
        result = filter[:keys].include?(user.name)
      else
        result = filter[:keys].index?(user.name) == nil
      end
    end
    result
  end

end