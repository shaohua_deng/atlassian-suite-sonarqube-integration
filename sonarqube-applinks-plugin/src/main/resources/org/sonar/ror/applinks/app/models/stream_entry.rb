#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class StreamEntry

  attr_reader :uuid, :title, :title_as_text, :link, :published
  attr_accessor :author, :category, :icon, :content

  def initialize(uuid, title, title_as_text, published, link)
    @uuid = uuid
    @title = title
    @title_as_text = title_as_text
    @published = get_time(published)
    @link = link
  end

  private

  # Returns a Time object
  def get_time(object)
    if object.is_a? Time
      object
    elsif object.is_a? DateTime
      Time.parse(object.to_s)
    elsif object.is_a? Java::JavaUtil::Date
      Time.at(object.getTime/1000)
    else
      raise(ArgumentError, 'object must be a Time, DateTime or Java::JavaUtil::Date object')
    end
  end

end
