#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class EntityLink < ActiveRecord::Base

  belongs_to :application_link, :class_name => 'ApplicationLink'

  named_scope :with_application_id, lambda { |value| {:conditions => {:application_id => value}} }
  named_scope :with_key, lambda { |value| {:conditions => {:kee => value}} }
  named_scope :with_type_id, lambda { |value| {:conditions => {:type_id => value}} }
  named_scope :with_project_id, lambda { |value| {:conditions => {:project_id => value}} }

  def self.clear(project_id, type_id = nil, key = nil, application_id = nil)
    all(project_id, type_id, key, application_id).delete_all
  end

  def self.all(project_id, type_id = nil, key = nil, application_id = nil)
    EntityLink.with_project_id(project_id).with_type_id(type_id).with_key(key).with_application_id(application_id)
  end

  def key
    kee
  end

  def primary
    primare
  end

  def is_primary?
    primare
  end

  def application_link
    ApplicationLink.with_application_id(application_id)
  end

  def project
    Project.by_key(project_id)
  end

  def to_xml(xml = Builder::XmlMarkup.new(:indent => 0))
    xml.entityLink do
      xml.applicationId(application_id)
      xml.typeId(type_id)
      xml.name(name)
      xml.key(key)
      xml.isPrimary(primary)
      xml.project do
        xml.id(project_id)
        xml.key(project.key)
        xml.name(project.name)
        xml.longname(project.long_name)
      end
    end
  end

end
