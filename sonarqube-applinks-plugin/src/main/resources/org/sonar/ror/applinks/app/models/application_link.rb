#
# SonarQube Applinks Plugin
# Copyright (C) 2013 Marvelution
# info@marvelution.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

class ApplicationLink < ActiveRecord::Base

  include Comparable

  named_scope :with_application_id, lambda { |value| {:conditions => {:application_id => value}} }

  def self.clear(application_id)
    all(application_id).delete_all
  end

  def self.all(application_id)
    ApplicationLink.with_application_id(application_id)
  end

  def primary
    primare
  end

  def is_primary?
    primare
  end

  def is_system?
    system
  end

  def entity_links
    EntityLink.with_application_id(application_id)
  end

  def <=>(other)
    application_id <=> other.application_id
  end

  def to_xml(xml = Builder::XmlMarkup.new(:indent => 0))
    xml.applicationLink do
      xml.id(application_id)
      xml.typeId(type_id)
      xml.name(name)
      xml.displayUrl(display_url)
      xml.rpcUrl(rpc_url)
      xml.iconUrl(icon_url)
      xml.isPrimary(primary)
      xml.isSystem(system)
      xml.entityLinks do
        entity_links.each do |entity|
          entity.to_xml(xml)
        end
      end
    end
  end

end
