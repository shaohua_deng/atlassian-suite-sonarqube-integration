/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.web.ServletFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;

/**
 * {@link ServletFilter} to filter for the Activity Streams Servlet requests
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class StreamsServletFilter extends ServletFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(StreamsServletFilter.class);
	private static final String STREAMS_URL = "/plugins/servlet/streams";
	private static final String API_STREAMS_FEED = "/api/streams/feed";

	@Override
	public UrlPattern doGetPattern() {
		return UrlPattern.create(STREAMS_URL + "*");
	}

	/**
	 * See {@link javax.servlet.Filter#init(javax.servlet.FilterConfig)}
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/**
	 * See {@link javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)}
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		final HttpServletRequest originalRequest = (HttpServletRequest) request;
		LOGGER.debug("Streams request {} [PathInfo: {}]", originalRequest.getRequestURI(), originalRequest.getPathInfo());
		final String feedURI = originalRequest.getRequestURI().replace(STREAMS_URL, API_STREAMS_FEED);
		LOGGER.info("Redirecting Streams request {} to {}", originalRequest.getRequestURI(), feedURI);
		HttpServletRequestWrapper httpRequest = new HttpServletRequestWrapper(originalRequest) {

			@Override
			public String getRequestURI() {
				return feedURI;
			}

			@Override
			public String getQueryString() {
				return super.getQueryString().replaceAll("((\\?|&)(sonarqube|streams)=)", "$2$3%5B%5D=");
			}

		};
		chain.doFilter(httpRequest, response);
	}

	/**
	 * See {@link javax.servlet.Filter#destroy()}
	 */
	public void destroy() {
	}

}
