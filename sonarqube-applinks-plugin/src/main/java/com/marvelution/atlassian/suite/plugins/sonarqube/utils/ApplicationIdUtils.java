/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.ServerExtension;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

/**
 * ApplicationId utility class
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class ApplicationIdUtils implements ServerExtension {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationIdUtils.class);

	/**
	 * Get an ApplicationId from the given URL
	 *
	 * @param url the URL to get an Id for
	 * @return the ApplicationId;
	 */
	public String getApplicationId(String url) {
		return getApplicationId(URI.create(url));
	}

	/**
	 * Get an ApplicationId from the given {@link URI}
	 *
	 * @param url the {@link URI} to get an Id for
	 * @return the ApplicationId;
	 */
	public String getApplicationId(URI url) {
		String normalisedUri = url.normalize().toASCIIString();
		while ((normalisedUri.endsWith("/")) && (normalisedUri.length() > 1)) {
			normalisedUri = normalisedUri.substring(0, normalisedUri.length() - 1);
		}
		return UUID.nameUUIDFromBytes(normalisedUri.getBytes()).toString();
	}

	/**
	 * Verify that hte given Id matches the Id that manifest services will provide from the given url
	 *
	 * @param id the Id to verify
	 * @param url the base url of the remote applinks system to get he manifest from
	 * @return {@code true} if the ids match, {@code false} otherwise
	 */
	public boolean verifyRemoteApplicationId(String id, String url) {
		try {
			LOGGER.debug("Verifying id {} on remote {}", id, url);
			URLConnection connection = new URL(StringUtils.stripEnd(url, "/") + "/rest/applinks/1.0/manifest")
					.openConnection();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document manifest = builder.parse(connection.getInputStream());
			XPath xPath = XPathFactory.newInstance().newXPath();
			String remoteId = xPath.compile("/manifest/id").evaluate(manifest);
			LOGGER.debug("Loaded remote manifest from {}; id: {}, remoteId: {}", new String[] { url, id, remoteId });
			if (remoteId != null && remoteId.equalsIgnoreCase(id)) {
				return true;
			}
		} catch (IOException e) {
			LOGGER.error("Failed to load manifest from {} [{}]", url, e.getMessage());
		} catch (Exception e) {
			LOGGER.error("Failed to parse manifest from {} [{}]", url, e.getMessage());
		}
		return false;
	}

}
