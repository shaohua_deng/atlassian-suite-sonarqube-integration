/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube;

import com.google.common.collect.ImmutableList;
import com.marvelution.atlassian.suite.plugins.sonarqube.filter.ApplinksAuthConfServletFilter;
import com.marvelution.atlassian.suite.plugins.sonarqube.filter.RestServletFilter;
import com.marvelution.atlassian.suite.plugins.sonarqube.filter.StreamsServletFilter;
import com.marvelution.atlassian.suite.plugins.sonarqube.utils.ApplicationIdUtils;
import org.slf4j.LoggerFactory;
import org.sonar.api.SonarPlugin;
import org.sonar.updatecenter.common.PluginManifest;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.Manifest;

/**
 * Customer {@link SonarPlugin} to register application link features
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public final class ApplinksPlugin extends SonarPlugin {

	private static PluginManifest PLUGIN_MANIFEST;

	@Override
	public List getExtensions() {
		return ImmutableList.builder().add(RestServletFilter.class, ApplinksAuthConfServletFilter.class, ApplinksSettings.class,
				ApplicationIdUtils.class, StreamsServletFilter.class).addAll(ApplinksSettings.getPropertyDefinitions()).build();
	}

	/**
	 * Get the {@link PluginManifest}
	 *
	 * @return the {@link PluginManifest}
	 */
	public static PluginManifest getManifest() {
		if (PLUGIN_MANIFEST == null) {
			try {
				Enumeration<URL> resources = ApplinksPlugin.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
				while (resources.hasMoreElements()) {
					PLUGIN_MANIFEST = new PluginManifest(new Manifest(resources.nextElement().openStream()));
					if (PLUGIN_MANIFEST.isValid()) {
						break;
					}
				}
			} catch (IOException e) {
				LoggerFactory.getLogger(ApplinksPlugin.class).error("Failed loading PluginManifest" , e);
			}
		}
		return PLUGIN_MANIFEST;
	}

}
