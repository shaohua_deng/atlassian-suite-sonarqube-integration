/*
 * SonarQube Applinks Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.atlassian.suite.plugins.sonarqube.filter;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.web.ServletFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link ServletFilter} to filter for the REST requests from Atlassian products and redirecting them to SonarQube API service
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class RestServletFilter extends ServletFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestServletFilter.class);
	private final Map<String, String> REST_TO_API = ImmutableMap.of("applinks", "api/applinks", "activity-stream", "api/streams");
	private Pattern uriPattern;

	@Override
	public UrlPattern doGetPattern() {
		return UrlPattern.create("/rest/*");
	}

	/**
	 * See {@link javax.servlet.Filter#init(javax.servlet.FilterConfig)}
	 */
	public void init(FilterConfig filterConfig) throws ServletException {
		uriPattern = Pattern.compile("^/(rest/([^/]+)/([^/]+))/(.*)");
	}

	/**
	 * See {@link javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)}
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String requestURI = httpRequest.getRequestURI();
		LOGGER.debug("Got a REST request at " + requestURI);
		Matcher uriMatcher = uriPattern.matcher(requestURI);
		if (uriMatcher.find()) {
			final String service = uriMatcher.group(2);
			final String fullAction = uriMatcher.group(4);
			if (REST_TO_API.containsKey(service)) {
				final String newURI = requestURI.replace(uriMatcher.group(1), REST_TO_API.get(service));
				LOGGER.info("Located {} REST action {}, forwarding to {}", new String[] { service, fullAction, newURI });
				if (httpRequest.getPathInfo() != null) {
					final String pathInfo = httpRequest.getPathInfo().replace(uriMatcher.group(1), REST_TO_API.get(service));
					httpRequest = new HttpServletRequestWrapper(httpRequest) {

						@Override
						public String getPathInfo() {
							return pathInfo;
						}

						@Override
						public String getRequestURI() {
							return newURI;
						}

					};
				} else {
					final String servletPath = httpRequest.getServletPath();
					httpRequest = new HttpServletRequestWrapper(httpRequest) {

						@Override
						public String getServletPath() {
							return servletPath;
						}

						@Override
						public String getRequestURI() {
							return newURI;
						}

					};
				}
				chain.doFilter(httpRequest, response);
			} else {
				LOGGER.warn("Unsupported REST call {}", requestURI);
				((HttpServletResponse) response).sendError(HttpServletResponse.SC_BAD_REQUEST, "Unsupported REST call");
			}
		} else {
			LOGGER.debug("Unable to locate rest action for url {}, forwarding request to filter chain", requestURI);
			chain.doFilter(request, response);
		}
	}

	/**
	 * See {@link javax.servlet.Filter#destroy()}
	 */
	public void destroy() {
	}

}
